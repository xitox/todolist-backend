const express = require("express");
const querystring = require('querystring');
var json = require('./data/tdl.json');
const fs = require('fs');

const PORT = process.env.PORT || 3001;

const app = express();

const getLastId = () => {
    return Number(json[Object.keys(json).sort((a, b) => Number(a) - Number(b)).pop()].id);
}

app.get("/api", (req, res) => {
    res.json(json);
});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});

app.post("/addTask", (req, res) => {
    let rawData = '';
    req.on('data', (chunk) => {
        rawData += chunk;
    });
    req.on('end', () => {
        const today = new Date();
        const dd = String(today.getDate()).padStart(2, '0');
        const mm = String(today.getMonth() + 1).padStart(2, '0');
        const yyyy = today.getFullYear();
        const currentDate = dd + '/' + mm + '/' + yyyy;
        const parsedData = JSON.parse(rawData);
        const id = getLastId() + 1;
        const newjson = {
            "id": String(id),
            "title": parsedData?.title,
            "description": parsedData?.description,
            "state": "DONE",
            "date": currentDate,
        }
        json[id] = newjson;
        const jsonContent = JSON.stringify(json);
        fs.writeFile("./server/data/tdl.json", jsonContent, 'utf8', (err) => {
            if (err) {
                console.log("An error occured while writing JSON Object to File.");
                return console.log(err);
            }

            console.log("JSON file has been saved.");
        });
    });
    res.sendStatus(200);
})


app.post("/deleteTask", (req, res) => {
    let rawData = '';
    req.on('data', (chunk) => {
        rawData += chunk;
    });
    req.on('end', async () => {
        const parsedData = JSON.parse(rawData);
        console.log(parsedData);
        delete json[parsedData?.id];
        const jsonContent = JSON.stringify(json);
        fs.writeFile("./server/data/tdl.json", jsonContent, 'utf8', (err) => {
            if (err) {
                console.log("An error occured while writing JSON Object to File.");
                return console.log(err);
            }

            console.log("JSON file has been saved.");
        });
    });
    res.sendStatus(200);
})